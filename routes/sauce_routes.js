var express = require('express');
var router = express.Router();
var sauce_dal = require('../model/sauce_dal');


// View all sauces
router.get('/all', function(req, res) {
    sauce_dal.getAll(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            var successMessage = req.query.successMessage;
            res.render('sauce/sauceViewAll', {'sauce': result, 'successMessage': successMessage});
        }
    });
});


// View sauce by id
router.get('/', function(req, res) {
    if(req.query.sauce_id == null) {
        res.send('sauce_id is null');
    }
    else {
        sauce_dal.getById(req.query.sauce_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                res.render('sauce/sauceViewById', {'sauce': result[0][0], 'pepper': result[1], 'average': result[2][0]});
            }
        });
    }
});


// Return the add new sauce form
router.get('/add', function(req, res) {
    sauce_dal.getPeppers(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('sauce/sauceAdd', {'pepper': result});
        }
    });
});


// Insert new sauce
router.get('/insert', function(req, res) {
    if(req.query.name == null || req.query.scoville == null) {
        res.send('All fields must be provided.');
    }
    else {
        sauce_dal.insert(req.query, function() {
            sauce_dal.insertSales(function() {
                res.redirect(302, '/sauce/all?successMessage=add');
            });
        });
    }
});


// Delete a sauce
router.get('/delete', function(req, res) {
    if(req.query.sauce_id == null) {
        res.send('sauce_id is null');
    }
    else {
        sauce_dal.delete(req.query.sauce_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/sauce/all?successMessage=delete');
            }
        });
    }
});


// Return the update sauce form
router.get('/edit', function(req, res) {
    if(req.query.sauce_id == null) {
        res.send('sauce_id is null');
    }
    else {
        sauce_dal.edit(req.query.sauce_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                var successMessage = req.query.successMessage;
                res.render('sauce/sauceUpdate', {'sauce': result[0][0], 'pepper': result[1], 'stock': result[2], 'store': result[3], 'successMessage': successMessage});
            }
        });
    }
});


// Update a sauce
router.get('/update', function(req, res) {
    sauce_dal.update(req.query, function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/sauce/all?successMessage=update');
        }
    });
});


// Update in stock # for each store
router.get('/stockUpdate', function(req, res) {
    sauce_dal.stockUpdate(req.query, function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/sauce/edit/?sauce_id=' + req.query.sauce_id + '&successMessage=update');
        }
    });
});


module.exports = router;