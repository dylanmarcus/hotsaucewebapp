var express = require('express');
var router = express.Router();
var store_dal = require('../model/store_dal');


// View all stores
router.get('/all', function(req, res) {
    store_dal.getAll(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            var successMessage = req.query.successMessage;
            res.render('store/storeViewAll', {'store': result, 'successMessage': successMessage});
        }
    });
});


// View store by id
router.get('/', function(req, res) {
    if(req.query.store_id == null) {
        res.send('store_id is null');
    }
    else {
        store_dal.getById(req.query.store_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                res.render('store/storeViewById', {'store': result[0][0], 'employee': result[1], 'sauce': result[2]});
            }
        });
    }
});


// Return the add new store form
router.get('/add', function(req, res) {
    store_dal.getAll(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('store/storeAdd');
        }
    });
});


// Insert new store
router.get('/insert', function(req, res) {
    if(req.query.phone == null || req.query.street == null ||
       req.query.city == null || req.query.state == null || req.query.zip == null) {
        res.send('All fields must be provided.');
    }
    else {
        store_dal.insert(req.query, function() {
            store_dal.insertStocks(function() {
                res.redirect(302, '/store/all?successMessage=add');
            });
        });
    }
});


// Delete a store
router.get('/delete', function(req, res) {
    if(req.query.store_id == null) {
        res.send('store_id is null');
    }
    else if(req.query.store_id >= 1 && req.query.store_id <= 3) {
        res.send('access denied for deleting original stores');
    }
    else {
        store_dal.delete(req.query.store_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/store/all?successMessage=delete');
            }
        });
    }
});


// Return edit store form
router.get('/edit', function(req, res) {
    if(req.query.store_id == null) {
        res.send('store_id is null');
    }
    else {
        store_dal.edit(req.query.store_id, function(err, result) {
            res.render('store/storeUpdate', {'store': result[0]});
        });
    }
});


// Update a store
router.get('/update', function(req, res) {
    store_dal.update(req.query, function() {
        res.redirect(302, '/store/all?successMessage=update');
    });
});


module.exports = router;