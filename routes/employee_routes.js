var express = require('express');
var router = express.Router();
var employee_dal = require('../model/employee_dal');
var store_dal = require('../model/store_dal');


// View all employees
router.get('/all', function(req, res) {
    employee_dal.getAll(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            var successMessage = req.query.successMessage;
            res.render('employee/employeeViewAll', {'employee': result, 'successMessage': successMessage});
        }
    });
});


// View employee by id
router.get('/', function(req, res) {
    if(req.query.employee_id == null) {
        res.send('employee_id is null');
    }
    else {
        employee_dal.getById(req.query.employee_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                res.render('employee/employeeViewById', {'employee': result[0][0], 'sauce': result[1]});
            }
        });
    }
});

// Return the add new employee form
router.get('/add', function(req, res) {
    store_dal.getAll(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('employee/employeeAdd', {'store': result});
        }
    });
});


// Insert new employee
router.get('/insert', function(req, res) {
    if(req.query.firstName == null || req.query.lastName == null ||
        req.query.phone == null || req.query.store_id == null) {
        res.send('All fields must be provided.');
    }
    else {
        employee_dal.insert(req.query, function() {
            employee_dal.insertSales(function() {
                res.redirect(302, '/employee/all?successMessage=add');
            });
        });
    }
});


// Delete an employee
router.get('/delete', function(req, res) {
    if(req.query.employee_id == null) {
        res.send('employee_id is null');
    }
    else {
        employee_dal.delete(req.query.employee_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/employee/all?successMessage=delete');
            }
        });
    }
});


// Return the update employee form
router.get('/edit', function(req, res) {
    if(req.query.employee_id == null) {
        res.send('employee_id is null');
    }
    else {
        employee_dal.edit(req.query.employee_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                var successMessage = req.query.successMessage;
                res.render('employee/employeeUpdate', {'employee': result[0][0], 'sauce': result[1], 'store': result[2], 'successMessage': successMessage});
            }
        });
    }
});


// Update an employee
router.get('/update', function(req, res) {
    employee_dal.update(req.query, function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/employee/all?successMessage=update');
        }
    });
});


// Update # of each sauce they've sold
router.get('/sauceUpdate', function(req, res) {
    employee_dal.sauceUpdate(req.query, function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/employee/edit/?employee_id=' + req.query.employee_id + '&successMessage=update');
        }
    });
});


module.exports = router;