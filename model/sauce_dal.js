var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


// Called to view all sauces
exports.getAll = function(callback) {
    var query = "SELECT * FROM sauce ORDER BY scoville;";

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

// Called to view all peppers
exports.getPeppers = function(callback) {
    var query = "SELECT DISTINCT pepper FROM sauce_pepper ORDER BY pepper;";

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


/* STORED PROCEDURE FOR GET BY ID
     drop procedure if exists sauce_getinfo;
     delimiter //
     create procedure sauce_getinfo (_sauce_id int)
     begin

     select sa.* from sauce sa where sa.sauce_id = _sauce_id;

     select sap.* from sauce_pepper sap where sap.sauce_id = _sauce_id
     order by sap.pepper;

     select name, fn_avg_sauce_sale(_sauce_id) as avg_sales from sauce
     where sauce_id = _sauce_id;

     end //
     delimiter ;


   FUNCTION USED BY PROCEDURE
     drop function if exists fn_avg_sauce_sale;
     delimiter //
     create function fn_avg_sauce_sale (_sauce_id int) returns double
     begin
     declare _avg_sale double;

     select avg(sold) into _avg_sale from employee_sauce
     where sauce_id = _sauce_id;

     return _avg_sale;
     end//
     delimiter ;
 */

// Called to view one sauce
exports.getById = function(sauce_id, callback) {
    var query = 'CALL sauce_getinfo(?);';
    var queryData = [sauce_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to add a sauce
exports.insert = function(params, callback) {
    var query =
        'INSERT INTO sauce (name, scoville) ' +
        'VALUES (?, ?);';

    var queryData = [params.name, params.scoville];

    connection.query(query, queryData, function(err, result) {
        var sauce_id = result.insertId;

        var query =
            'INSERT INTO sauce_pepper (sauce_id, pepper) ' +
            'VALUES ?';

        var saucePepperData = [];
        for (var i = 0; i < params.pepper.length; i++ ) {
            saucePepperData.push([sauce_id, params.pepper[i]]);
        }

        connection.query(query, [saucePepperData], function(err, result) {
            callback(err, result);
        });

    });
};


// Called when new sauce is added to set all sale values to 0
exports.insertSales = function(callback) {
    var query = 'CALL sauce_default_sales();';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


// Called to delete a sauce
exports.delete = function(sauce_id, callback) {
    var query = 'DELETE FROM sauce WHERE sauce_id = ?';
    var queryData = [sauce_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to edit a sauce
exports.edit = function(sauce_id, callback) {
    var query = 'CALL sauce_edit(?)';
    var queryData = [sauce_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

// Called to insert all sauce_pepper data
var saucePepperInsert = function(sauce_id, pepperArray, callback) {
    var query =
        'INSERT INTO sauce_pepper (sauce_id, pepper) ' +
        'VALUES ?';

    var saucePepperData = [];
    for (var i = 0; i < pepperArray.length; i++ ) {
        saucePepperData.push([sauce_id, pepperArray[i]]);
    }

    connection.query(query, [saucePepperData], function(err, result) {
        callback(err, result);
    });
};

module.exports.saucePepperInsert = saucePepperInsert;

// Called to delete all sauce_pepper data
var saucePepperDeleteAll = function(sauce_id, callback) {
    var query = 'DELETE FROM sauce_pepper WHERE sauce_id = ?';
    var queryData = [sauce_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

module.exports.saucePepperDeleteAll = saucePepperDeleteAll;

// Called to update a sauce
exports.update = function(params, callback) {
    var query = 'UPDATE sauce SET name = ?, scoville = ? WHERE sauce_id = ?';
    var queryData = [params.name, params.scoville, params.sauce_id];

    connection.query(query, queryData, function(err, result) {
        saucePepperDeleteAll(params.sauce_id, function(err, result) {

            if(params.pepper != null) {
                saucePepperInsert(params.sauce_id, params.pepper, function(err, result) {
                    callback(err, result);
                });
            }
            else {
                callback(err, result);
            }
        });
    });
};


// Called to update # of a sauces in stock
exports.stockUpdate = function(params, callback) {
    var query =
        'UPDATE store_sauce SET in_stock = ? ' +
        'WHERE store_id = ? AND sauce_id = ?';

    var queryData = [params.in_stock, params.store_id, params.sauce_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};