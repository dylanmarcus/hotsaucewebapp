var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


// Called to view all employees
exports.getAll = function(callback) {
    var query =
        'SELECT e.*, s.street FROM employee e ' +
        'JOIN store s ON e.store_id = s.store_id ' +
        'ORDER BY s.street, e.lastName;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


/* STORED PROCEDURE FOR GET BY ID
     drop procedure if exists employee_getinfo;
     delimiter //
     create procedure employee_getinfo (_employee_id int)
     begin

     select e.*, s.street from employee e
     left join store s on e.store_id = s.store_id
     where e.employee_id = _employee_id;

     create or replace view employee_sales as
     select sa.sauce_id, sa.name, esa.employee_id, esa.sold from sauce sa
     left join employee_sauce esa on sa.sauce_id = esa.sauce_id
     order by esa.sold desc;

     select * from employee_sales
     where employee_id = _employee_id;

     end //
     delimiter ;
 */

// Called to view one employee
exports.getById = function(employee_id, callback) {
    var query = 'CALL employee_getinfo(?);';
    var queryData = [employee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to add an employee
exports.insert = function(params, callback) {
    var query =
        'INSERT INTO employee (firstName, lastName, phone, store_id) ' +
        'VALUES (?, ?, ?, ?);';

    var queryData = [params.firstName, params.lastName, params.phone, params.store_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};



/* STORED PROCEDURE USED BY insertSales
     drop procedure if exists default_sales;
     delimiter //
     create procedure default_sales ()
     begin

     create or replace view entries as
     select (select max(employee_id) from employee), sa.sauce_id from sauce sa;

     insert into employee_sauce (employee_id, sauce_id)
     select * from entries;

     end //
     delimiter ;
 */

// Called when new employee is added to set all sale values to 0
exports.insertSales = function(callback) {
    var query = 'CALL default_sales();';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


// Called to delete an employee
exports.delete = function(employee_id, callback) {
    var query = 'DELETE FROM employee WHERE employee_id = ?';
    var queryData = [employee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to edit an employee
exports.edit = function(employee_id, callback) {
    var query = 'CALL employee_edit(?)';
    var queryData = [employee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to update an employee
exports.update = function(params, callback) {
    var query =
        'UPDATE employee ' +
        'SET firstName = ?, lastName = ?, phone = ?, store_id = ? ' +
        'WHERE employee_id = ?';

    var queryData = [params.firstName, params.lastName, params.phone, params.store_id, params.employee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to update # of a sauce sold
exports.sauceUpdate = function(params, callback) {
    var query =
        'UPDATE employee_sauce SET sold = ? ' +
        'WHERE sauce_id = ? AND employee_id = ?';

    var queryData = [params.sold, params.sauce_id, params.employee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};