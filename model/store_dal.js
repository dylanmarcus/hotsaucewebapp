var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

// Called to view all stores
exports.getAll = function(callback) {
    var query = 'SELECT * FROM store;';

    connection.query(query, function(err, result) {
        callback (err, result);
    });
};


/* STORED PROCEDURE FOR GET BY ID
     drop procedure if exists store_getinfo;
     delimiter //
     create procedure store_getinfo (_store_id int)
     begin

     select s.* from store s where store_id = _store_id;

     select e.* from employee e
     where e.store_id = _store_id
     order by e.lastName;

     select sa.sauce_id, sa.name, ss.in_stock, ss.store_id from sauce sa
     join store_sauce ss on sa.sauce_id = ss.sauce_id
     where ss.store_id = _store_id
     order by sa.name;

     end //
     delimiter ;
 */

// Called to view one store
exports.getById = function(store_id, callback) {
    var query = 'CALL store_getinfo(?)';
    var queryData = [store_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to add a store
exports.insert = function(params, callback) {
    var query =
        'INSERT INTO store (phone, street, city, state, zip) ' +
        'VALUES (?, ?, ?, ?, ?);';

    var queryData = [params.phone, params.street, params.city, params.state, params.zip];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called when new store is added to set all stock values to 0
exports.insertStocks = function(callback) {
    var query = 'CALL default_stock();';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


/* STORED PROCEDURE FOR DELETING A STORE
     drop procedure if exists store_delete;
     delimiter //
     create procedure store_delete (_store_id int)
     begin

     update employee
     set store_id = null
     where store_id = _store_id;

     delete from store where store_id = _store_id;

     end //
     delimiter ;
 */

// Called to delete a store
exports.delete = function(store_id, callback) {
    var query = 'CALL store_delete(?)';
    var queryData = [store_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

// Called to edit a store
exports.edit = function(store_id, callback) {
    var query = 'SELECT * FROM store WHERE store_id = ?';
    var queryData = [store_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


// Called to update a store
exports.update = function(params, callback) {
    var query =
        'UPDATE store SET phone = ?, street = ?, city = ?, state = ?, zip = ? ' +
        'WHERE store_id = ?;';
    var queryData = [params.phone, params.street, params.city, params.state, params.zip, params.store_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};